//
//  TabBarAppController.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class TabBarAppController: UITabBarController {
    
    
    // MARK: - init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.configTabBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configTabBar(){
        
        self.tabBar.barTintColor = Colors.BackgroundMedium
        self.tabBar.unselectedItemTintColor = Colors.lightGray
        self.tabBar.tintColor = Colors.DarkGray
    
    }
}
// MARK: - UITabBarControllerDelegate
extension TabBarAppController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected: \(viewController.title ?? "")")
    }
}
