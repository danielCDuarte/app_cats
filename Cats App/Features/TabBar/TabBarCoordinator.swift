//
//  TabBarCoordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class TabBarCoordinator: Coordinator {
    
    weak var rootViewController: UIViewController?
    
    
    init(rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        let tabBarController:TabBarAppController = TabBarAppController()
        
        // Create Tab BreedCats
        let navigationBreedCatsController = ScrollingNavigationController()
        let breedCatsCoordinator:BreedCatsCoordinator = BreedCatsCoordinator(rootViewController: navigationBreedCatsController)
        breedCatsCoordinator.start()
        tabBarController.viewControllers = [breedCatsCoordinator.rootViewController!]
        
        // Create Tab YouLike
        let navigationYouLikeController = ScrollingNavigationController()
        let youlikeCoordinator:YouLikeCoordinator = YouLikeCoordinator(rootViewController: navigationYouLikeController)
        youlikeCoordinator.start()
        tabBarController.viewControllers?.append(youlikeCoordinator.rootViewController!)
        
        // Create Tab MyCats
        let navigationMyCatsController = ScrollingNavigationController()
        let myCatsCoordinator:MyCatsCoordinator = MyCatsCoordinator(rootViewController: navigationMyCatsController)
        myCatsCoordinator.start()
        tabBarController.viewControllers?.append(myCatsCoordinator.rootViewController!)
        
        guard let navigationController = self.rootViewController as? UINavigationController else {
            fatalError("Should be a navigation")
        }
        navigationController.isNavigationBarHidden = true
        navigationController.pushViewController(tabBarController, animated: false)
    }
}

