//
//  MainCoordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    private let window: UIWindow
    weak var rootViewController: UIViewController?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let nav = UINavigationController()
        self.rootViewController = nav
        self.window.rootViewController = self.rootViewController
        let tabBarController:TabBarCoordinator = TabBarCoordinator(rootViewController: nav)
        tabBarController.start()
    }
}
