//
//  YouLikeViewController.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class YouLikeViewController: UIViewController{
    // MARK: - Properties
    private lazy var youLikeView: YouLikeView = {
        let view = YouLikeView(delegate: self)
        return view
    }()
    
    // MARK: - Init
    private let service: YouLikeService
    private var page = 0
    private var index = 0
    let userDetafult = UserDefaults.standard
    var youLikeList = [YouLikeModel]()
    
    init(service: YouLikeService = YouLikeService()) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view life cycle
    override func loadView() {
        super.loadView()
        self.view = youLikeView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.page = userDetafult.integer(forKey: "page")
        self.index = userDetafult.integer(forKey: "index")
        
        
        self.getFavorites()
    }
    
    func showMessageError(title: String, message: String, buttonTitle:String,_ completion: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
            completion()
        }))
        self.present(alert, animated: true)
        
    }
    
    //MARK: Request
    func getFavorites(){
        self.view.showBlurLoader()
        service.fetchAllFavorites(limit: Constants.PAGINATOR, page: self.page) { (result) in
            switch result{
            case .success(let response):
                if response.count >= 1{
                    DispatchQueue.main.async {
                        self.youLikeList = response
                        self.youLikeView.setup(youLikeModel: self.youLikeList[self.index])
                        self.view.removeBluerLoader()
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    self.showMessageError(title: NSLocalizedString("YouLikeViewController.errorTitle", comment: ""), message: NSLocalizedString("YouLikeViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("YouLikeViewController.btn", comment: "")) {
                    }
                }
                
            case .failure( _):
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    self.showMessageError(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""), message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
                    }
                }
            }
        }
    }
    
    //MARK: Paginator
    func validatePaginator(){
        userDetafult.set(self.index, forKey: "index")
        if index + 1 >= self.youLikeList.count{
            self.page += 1
            self.index = 0
            userDetafult.set(self.index, forKey: "page")
            userDetafult.set(self.page, forKey: "page")
            self.getFavorites()
            return
        }
        
        self.youLikeView.setup(youLikeModel: self.youLikeList[index])
        
    }
    
}

extension YouLikeViewController: YouLikeDelegate{
    func didItsLike(youLike: YouLikeModel) {
        self.view.showBlurLoader()
        self.service.saveFavoritePersistence(youLikeModel: youLike) { (state) in
            switch state{
            case true:
                self.index += 1
                self.view.removeBluerLoader()
                self.validatePaginator()
                
            case false:
                self.view.removeBluerLoader()
                self.showMessageError(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""), message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
                }
            }
        }
    }
    
}
