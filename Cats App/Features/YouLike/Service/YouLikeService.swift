//
//  YouLikeService.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import RealmSwift

class YouLikeService{
    private let apiClient:APIClient
    private let realm:Realm
    
    init(apiClient:APIClient = APIClient(),realm:Realm = try! Realm()) {
        self.apiClient = apiClient
        self.realm = realm
    }
    
    func fetchAllFavorites(limit:Int, page:Int, _ completion: @escaping ((Result<[YouLikeModel]>) -> Void)){
      
        let params = ["limit":String(limit), "page": String(page),"order": "DESC"]
        
        apiClient.load(.favorites, method: .get, headers: nil, params: params) { (result) in
            switch result {
            case .success(let data):
                do {
                    let items = try JSONDecoder().decode([YouLikeModel].self, from: data)
                    completion(.success(items))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func saveFavoritePersistence(youLikeModel:YouLikeModel, completion: @escaping ((Bool) -> Void)){
        let newLikeCat = LikeCatModel()
        newLikeCat.name = youLikeModel.imageId
        newLikeCat.createdAt = youLikeModel.createdAt ?? ""
        //format date
        newLikeCat.isLike = youLikeModel.isLike
        newLikeCat.url = youLikeModel.image?.url ?? ""
        do {
            try realm.write {
                realm.add(newLikeCat)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
}
