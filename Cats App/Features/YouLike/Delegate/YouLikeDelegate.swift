//
//  YouLikeDelegate.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

protocol YouLikeDelegate: class {
    func didItsLike(youLike:YouLikeModel)
}
