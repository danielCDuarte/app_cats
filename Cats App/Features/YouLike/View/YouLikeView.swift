//
//  YouLikeView.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit
import SnapKit

class YouLikeView: UIView {
    // MARK: - properties
    
    private(set) lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        return view
    }()
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    
    private(set) lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.fontDark
        label.font = fonts.fontSubTitle
        label.textAlignment = .center
        return label
    }()
    
    
    private lazy var photoImageView: UIImageView = {
        let photoImageVIew = UIImageView()
        photoImageVIew.contentMode = .scaleAspectFit
        photoImageVIew.image = UIImage(named: "roundBreedMedium")
        return photoImageVIew
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private(set) lazy var likeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(actionLike(_:)), for: .touchUpInside)
        button.setImage(UIImage(named: "happy"), for: .normal)
        return button
    }()
    
    
    private(set) lazy var disLikeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(actionDisLike(_:)), for: .touchUpInside)
        button.setImage(UIImage(named: "close"), for: .normal)
        return button
    }()
    
    
    // MARK: - init
    private weak var delegate: YouLikeDelegate?
    
    init(delegate: YouLikeDelegate?) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - setup
    var youLikeModel:YouLikeModel?
    func setup(youLikeModel:YouLikeModel){
        self.youLikeModel = youLikeModel
        self.nameLabel.text = youLikeModel.imageId
        self.photoImageView.image = UIImage(named: "roundBreedMedium")
        if let urlString = youLikeModel.image?.url{
            self.photoImageView.downloaded(from: urlString)
        }
        
    }
    
    
    // MARK: - Actions
    
    @objc func actionLike(_ sender : UIButton){
        if let youLikeModelValidate = youLikeModel{
            youLikeModelValidate.isLike = true
            self.delegate?.didItsLike(youLike: youLikeModelValidate)
        }
        
    }
    
    @objc func actionDisLike(_ sender : UIButton){
        if let youLikeModelValidate = youLikeModel{
            youLikeModelValidate.isLike = false
            self.delegate?.didItsLike(youLike: youLikeModelValidate)
        }
        
    }
    
}


// MARK: - View Configuration
extension YouLikeView: ViewConfiguration {
    
    func configureViews() {
        self.backgroundColor = Colors.BackgroundMedium
    }
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(photoImageView)
        containerView.addSubview(footerStackView)
        footerStackView.addArrangedSubview(disLikeButton)
        footerStackView.addArrangedSubview(likeButton)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(.low)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(100)
        }
        
        photoImageView.snp.makeConstraints{ (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(16)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(250)
        }
        
        footerStackView.snp.makeConstraints { (make) in
            make.top.equalTo(photoImageView.snp.bottom).offset(16)
            make.height.equalTo(150)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
        
    }
}
