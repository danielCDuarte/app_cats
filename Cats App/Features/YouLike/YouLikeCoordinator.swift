//
//  YouLikeCoordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import UIKit

class YouLikeCoordinator: Coordinator {
    
    weak var rootViewController: UIViewController?
    private let navigationController: ScrollingNavigationController
    
    init(rootViewController: ScrollingNavigationController) {
        self.navigationController = rootViewController
        self.rootViewController = navigationController
        
    }
    
    func start() {
        let youLikeViewController = YouLikeViewController()
        youLikeViewController.title =  NSLocalizedString("BreedCatsListViewController.YouLike", comment: "")
        let youLikeBarItem = UITabBarItem(title: nil, image: UIImage(named: "heartHalfTab"), selectedImage: UIImage(named: "heartHalfTab"))
        youLikeBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        youLikeViewController.tabBarItem = youLikeBarItem
        
        guard let navigationController = self.rootViewController as? UINavigationController else {
            fatalError("Should be a navigation")
        }
        navigationController.pushViewController(youLikeViewController, animated: false)
    }
}
