//
//  YouLikeModel.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

class YouLikeModel : Codable{
    var id: Int
    var createdAt:String?
    var imageId:String
    var image:YouLikeImageModel?
    var isLike:Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case createdAt = "created_at"
        case imageId = "image_id"
        case image = "image"
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        createdAt = try values.decode(String.self, forKey: .createdAt)
        imageId = try values.decode(String.self, forKey: .imageId)
        image = try values.decode(YouLikeImageModel.self, forKey: .image)
    }
    
    init(id: Int, createdAt:String?, imageId:String,image:YouLikeImageModel?,isLike:Bool) {
        self.id = id
        self.createdAt = createdAt
        self.imageId = imageId
        self.image = image
        self.isLike = isLike
    }
    
}

class YouLikeImageModel : Codable{
    var url: String
    enum CodingKeys: String, CodingKey {
        case url = "url"
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decode(String.self, forKey: .url)
    }
    
    init(url: String) {
        self.url = url
    }
}
