//
//  LikeCatModel.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import RealmSwift

class LikeCatModel: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var createdAt: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var isLike: Bool = false
}
