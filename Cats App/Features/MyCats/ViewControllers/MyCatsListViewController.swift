//
//  MyCatsListViewController.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class MyCatsListViewController: UIViewController{
    // MARK: - Properties
    var myCatsList = [LikeCatModel]()
    private lazy var myCatsListView: MyCatsListView = {
        let view = MyCatsListView()
        return view
    }()
    
    // MARK: - Init
    private let service: MyCatsService = MyCatsService()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view life cycle
    override func loadView() {
        super.loadView()
        self.view = myCatsListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getMyCats()
    }
    
    func showMessageError(title: String, message: String, buttonTitle:String,_ completion: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
            completion()
        }))
        self.present(alert, animated: true)
        
    }
    
    //MARK: Request
    
    func getMyCats(){
        self.view.showBlurLoader()
        service.fetchAllMyCats { (result) in
            switch result{
                case .success(let response):
                DispatchQueue.main.async {
                    self.myCatsList = response
                    self.myCatsListView.setup(myCatsList: self.myCatsList)
                    self.view.removeBluerLoader()
                }
                case .failure( _):
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    self.showMessageError(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""), message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
                        
                    }
                }
            }
        }
    }
    
    
}
