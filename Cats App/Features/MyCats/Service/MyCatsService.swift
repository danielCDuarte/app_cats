//
//  MyCatsService.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import RealmSwift

class MyCatsService{

    func fetchAllMyCats(completion: @escaping ((Result<[LikeCatModel]>) -> Void)){
        do {
            let list = try! Realm().objects(LikeCatModel.self)
            var listMyCats:[LikeCatModel] = [LikeCatModel]()
            for item in list {
                listMyCats.append(item)
            }
            completion(.success(listMyCats))

        } catch let error {
            completion(.failure(error))
        }
    }
}
