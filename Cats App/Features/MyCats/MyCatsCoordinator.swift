//
//  MyCatsCoordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import UIKit

class MyCatsCoordinator: Coordinator {
    
    weak var rootViewController: UIViewController?
    private let navigationController: ScrollingNavigationController
    
    init(rootViewController: ScrollingNavigationController) {
        self.navigationController = rootViewController
        self.rootViewController = navigationController
        
    }
    
    func start() {
        let myCatsListViewController = MyCatsListViewController()
        myCatsListViewController.title = NSLocalizedString("BreedCatsListViewController.MyCats", comment: "")
        let myCatsListBarItem = UITabBarItem(title: nil, image: UIImage(named: "catOnlineTab"), selectedImage: UIImage(named: "catOnlineTab"))
        myCatsListBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        myCatsListViewController.tabBarItem = myCatsListBarItem
        
        guard let navigationController = self.rootViewController as? UINavigationController else {
            fatalError("Should be a navigation")
        }
        navigationController.pushViewController(myCatsListViewController, animated: false)
    }
}
