//
//  BreedCatsService.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

class BreedCatsService{
    private let apiClient:APIClient
    
    init(apiClient:APIClient = APIClient()) {
        self.apiClient = apiClient
    }
    
    func fetchAllBreeds(limit:Int, page:Int, _ completion: @escaping ((Result<[BreedModel]>) -> Void)){
        
        let params = ["limit":String(limit), "page": String(page)]
        
        apiClient.load(.allBreeds, method: .get, headers: nil, params:params) { (result) in
            switch result {
            case .success(let data):
                do {
                    let items = try JSONDecoder().decode([BreedModel].self, from: data)
                    completion(.success(items))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchDetailBreed(breed:BreedModel, _ completion: @escaping ((Result<BreedDetailModel>) -> Void)){
        
        let params = ["breed_ids":breed.id]
        
        apiClient.load(.detailBreed, method: .get, headers: nil, params: params) { (result) in
            switch result{
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode([BreedDetailModel].self, from: data)
                    if let firsElement = result.first{
                        completion(.success(firsElement))
                    }
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
}
