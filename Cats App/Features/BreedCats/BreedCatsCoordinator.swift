//
//  BreedCatsCoordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import UIKit

class BreedCatsCoordinator: Coordinator {
    
    weak var rootViewController: UIViewController?
    private let navigationController: ScrollingNavigationController
    
    init(rootViewController: ScrollingNavigationController) {
        self.navigationController = rootViewController
        self.rootViewController = navigationController
        
    }
    
    func start() {
        let tabBreedCatsViewController = BreedCatsListViewController()
        tabBreedCatsViewController.title =  NSLocalizedString("BreedCatsListViewController.BreedCats", comment: "")
        let tabBreedCatsBarItem = UITabBarItem(title: nil, image: UIImage(named: "footPrintTab"), selectedImage: UIImage(named: "footPrintTab"))
        tabBreedCatsBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        tabBreedCatsViewController.tabBarItem = tabBreedCatsBarItem
        
        guard let navigationController = self.rootViewController as? UINavigationController else {
            fatalError("Should be a navigation")
        }
        navigationController.pushViewController(tabBreedCatsViewController, animated: false)
    }
}


