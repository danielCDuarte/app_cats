//
//  BreedCatsDetailViewController.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class BreedCatsDetailViewController: UIViewController{
    // MARK: - Properties
    
    private lazy var breedCatsDetailView: BreedCatsDetailView = {
        let view = BreedCatsDetailView(delegate: self)
        return view
    }()
    
    
    // MARK: - Init
    var breedDetail: BreedDetailModel
    init(breedDetail: BreedDetailModel) {
        self.breedDetail = breedDetail
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = breedDetail.breeds.first?.name
        self.view = breedCatsDetailView
        breedCatsDetailView.setupView(breedDetail: self.breedDetail)
    }
    
}

extension BreedCatsDetailViewController: BreedCatsDetailViewDelegate{
    func didOpenWiki(url: URL) {
        UIApplication.shared.open(url)
    }
    
}
