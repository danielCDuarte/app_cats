//
//  BreedCatsListViewController.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

class BreedCatsListViewController: UIViewController{
    
    // MARK: - Properties
    var breedList = [BreedModel]()
    private lazy var breedCatsListView: BreedCatsListView = {
        let view = BreedCatsListView(pagenatorIndex: 0, limit: Constants.PAGINATOR, delegate: self)
        return view
    }()
    
    // MARK: - Init
    private let service: BreedCatsService
    private var page:Int = 0
    init(service: BreedCatsService = BreedCatsService()) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view life cycle
    override func loadView() {
        super.loadView()
        self.view = breedCatsListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getBreeds(limit: Constants.PAGINATOR, page: self.page)
    }
    
    func showMessageError(title: String, message: String, buttonTitle:String,_ completion: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
            completion()
        }))
        self.present(alert, animated: true)
        
    }
    
    
    //MARK: Request
    
    func getBreeds(limit: Int, page: Int) {
        self.view.showBlurLoader()
        
        self.service.fetchAllBreeds(limit: limit, page: page) { (result) in
            switch result{
            case .success(let response):
                if response.count >= 1{
                    DispatchQueue.main.async {
                        self.breedList.append(contentsOf: response)
                        self.breedCatsListView.setup(breedList: self.breedList)
                        self.view.removeBluerLoader()
                    }
                    return
                }
                
            case .failure( _):
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    self.showMessageError(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""), message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
                    }
                }
            }
        }
        
        
    }
    
    func getDetailBreed(breed: BreedModel) {
        self.view.showBlurLoader()
        self.service.fetchDetailBreed(breed: breed) { (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    let breedCatsDetailVC:BreedCatsDetailViewController = BreedCatsDetailViewController(breedDetail: response)
                    self.navigationController?.pushViewController(breedCatsDetailVC, animated: true)
                }
            case .failure( _):
                DispatchQueue.main.async {
                    self.view.removeBluerLoader()
                    self.showMessageError(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""), message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""), buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
                        
                    }
                }
            }
        }
        
    }
    
}

extension BreedCatsListViewController: BreedCatsListViewDelegate{
    func didPaginator(limit: Int, page: Int) {
        self.page = page
        self.getBreeds(limit: limit, page: page)
    }
    
    func didSelect(breed: BreedModel) {
        self.getDetailBreed(breed: breed)
    }
    
}
