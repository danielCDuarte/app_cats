//
//  BreedCatsListViewDelegate.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

protocol BreedCatsListViewDelegate: class {
    func didSelect(breed:BreedModel)
    func didPaginator(limit:Int, page:Int)
}

