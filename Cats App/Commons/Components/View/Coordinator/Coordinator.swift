//
//  Coordinator.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//
import UIKit

protocol Coordinator {
    var rootViewController: UIViewController? {get}
    func start()
}
