//
//  ViewConfiguration.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

public protocol ViewConfiguration: AnyObject {
    func buildViewHierarchy()
    func setupConstraints()
    func configureViews()
    func setupViewConfiguration()
}

public extension ViewConfiguration {
    func setupViewConfiguration() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    func configureViews() {
    }
}
