//
//  Fonts.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import UIKit

struct fonts {
    static let fontNavTitle:UIFont = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!
    static let fontParagraph:UIFont = UIFont(name: "AppleSDGothicNeo-Thin", size: 14)!
    static let fontSubTitle:UIFont = UIFont(name: "AppleSDGothicNeo-UltraLight", size: 14)!
    static let fontButton:UIFont = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)!
}
