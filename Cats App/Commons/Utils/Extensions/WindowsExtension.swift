//
//  WindowsExtension.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    
    static var testWindow: UIWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: 467, height: 800))
    static var testSizes = [
        "SmallSize": CGSize(width: 375, height: 667),
        "MediumSize": CGSize(width: 357, height: 816),
        "LargeSize": CGSize(width: 414, height: 732)]
    
    static func setTestWindow(rootViewController: UIViewController) {
        self.testWindow.rootViewController = rootViewController
        self.testWindow.makeKeyAndVisible()
    }
    
    static func cleanTestWindow() {
        self.testWindow.rootViewController = nil
        self.testWindow.isHidden = true
    }
}
