//
//  ScrollingNavigationController+UIGestureRecognizerDelegate.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit

extension ScrollingNavigationController: UIGestureRecognizerDelegate {
    
    // MARK: - UIGestureRecognizerDelegate
    /**
     UIGestureRecognizerDelegate function. Enables the scrolling of both the content and the navigation bar
     */
    open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /**
     UIGestureRecognizerDelegate function. Only scrolls the navigation bar with the content when `scrollingEnabled` is true
     */
    open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return scrollingEnabled
    }
    
}
