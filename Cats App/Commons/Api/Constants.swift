//
//  Constants.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

struct Constants{
    //base url
    static let BASEURL:String = "https://api.thecatapi.com/v1/"
    static let APIKEY:String = "DEMO-API-KEY"
    static let PAGINATOR:Int = 20
   
}


enum enumServices: String {
    case allBreeds = "breeds"
    case detailBreed = "images/search"
    case favorites = "favourites"
}

