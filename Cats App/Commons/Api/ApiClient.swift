//
//  ApiClient.swift
//  Cats App
//
//  Created by Daniel Crespo Duarte on 15/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Foundation

enum HttpMethods: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
}

enum Result<T> {
    case success(T)
    case failure(Error)
}

enum APIClientError: Error {
    case noData
}

struct Resource {
    let url: URL
    let method: HttpMethods
}


final class APIClient {
    var dataTask: URLSessionDataTask?
    let defaultSession = URLSession(configuration: .default)
    
    func load(_ serviceToCall: enumServices, method:HttpMethods ,headers: [String : String]?, params: [String : String]?,result: @escaping ((Result<Data>) -> Void)) {
        
        dataTask?.cancel()
        
        var urlString:String = "\(Constants.BASEURL)\(serviceToCall.rawValue)"
        
        if (method == .get){
            if let paramsValidate = params{
                urlString.append("?\(paramsValidate.queryString)")
            }
        }
        
        guard let url:URL = URL(string: urlString) else {
            return
        }
        
        let resource = Resource(url: url, method: method)
        
        var request = URLRequest(resource)
        
        
        if let paramsValidate = params{
            if (method == .post){
                request.httpBody = try! JSONSerialization.data(withJSONObject: paramsValidate, options: [])
            }
            
        }
        //headers base
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(Constants.APIKEY, forHTTPHeaderField: "x-api-key")
        
        if let headersValidate = headers{
            for (key, value) in headersValidate {
                request.setValue(value, forHTTPHeaderField:key)
            }
        }
        
        dataTask = defaultSession.dataTask(with: request) { (data, response, error) in
            defer {
                self.dataTask = nil
            }
            
            guard let `data` = data else {
                result(.failure(APIClientError.noData))
                return
            }
            if let `error` = error {
                result(.failure(error))
                return
            }
            result(.success(data))
        }
        dataTask?.resume()
    }
    
    
}
