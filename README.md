# App_cats

App Cats proyecto creado en swift 5 y con compatibilidad para dispositivos ios desde la version 11.0

**Instalacion**
1. Descargue el repo e instale las dependencias implementadas por pods con el comando: `pod install`.
1. Abra el archivo Cats App.xcworkspace en la aplicación de xcode 11 o superior. 
1. Si desea probar la app seleccione el scheme Cats App y compile el proyecto.
1. Si desea probar el proyecto de unit test seleccione el scheme Cats AppTests y compile el proyecto actualmente tiene un coverage de 44.1% .

**Arquitectura**

El proyecto se implemento con arquitectura MVC con Coordinators utilizando vistas UI de tipo viewCode desacopladas. 
Se implementaron buenas practicas de desarrollo  siguiendo los pilares de oop, protocols, clean code y 
se utilizaron las dependecias necesarias creando componentes reutilizables y escalables a futuro.  
Se implemento la capa de consumo de ws utilizando urlSession.
En la parte de persistencia de datos se utilizo la lib de `realm`, para guardar banderillas se implemento `userDefaults`.

Se realizaron unit test utilizando las lib `Nimble-Snapshots`, `Quick` en las cuales se realizaron pruebas funcionales y visuales llegando 
a un coverage de 44.1%.
