//
//  YouLikeViewSpec.swift
//  Cats AppTests
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import Cats_App

class YouLikeViewSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsDetailViewSpec") {
            
            var sut: YouLikeView!
            var mainVC: UIViewController!
            beforeEach {
                var delegate: YouLikeDelegate?
                let youLike:YouLikeModel = YouLikeModel(id: 2027090, createdAt: "2020-08-16T19:51:55.000Z", imageId: "MTgxNTk2MQ", image: YouLikeImageModel(url: "https://cdn2.thecatapi.com/images/MTgxNTk2MQ.jpg"), isLike: true)
                
                sut = YouLikeView(delegate: delegate)
                sut.setup(youLikeModel: youLike)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("YouLikeViewSpec")
                })
            })
        }
    }
}
