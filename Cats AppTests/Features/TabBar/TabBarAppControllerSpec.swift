//
//  TabBarAppControllerSpec.swift
//  Cats AppTests
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import UIKit
import Quick
import Nimble
import Nimble_Snapshots
@testable import Cats_App

class TabBarAppControllerSpec: QuickSpec {

    override func spec() {
        describe("TabBarAppControllerSpec") {
            var sut: TabBarAppController!

            beforeEach {

                let oneViewController = UIViewController()
                let tabBreedCatsBarItem = UITabBarItem(title: nil, image: UIImage(named: "footPrintTab"), selectedImage: UIImage(named: "footPrintTab"))
                tabBreedCatsBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
                oneViewController.tabBarItem = tabBreedCatsBarItem
                
                let twoViewController = UIViewController()
                let youLikeBarItem = UITabBarItem(title: nil, image: UIImage(named: "heartHalfTab"), selectedImage: UIImage(named: "heartHalfTab"))
                youLikeBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
                twoViewController.tabBarItem = youLikeBarItem
                
                let threeViewController = UIViewController()
                let myCatsListBarItem = UITabBarItem(title: nil, image: UIImage(named: "catOnlineTab"), selectedImage: UIImage(named: "catOnlineTab"))
                myCatsListBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
                threeViewController.tabBarItem = myCatsListBarItem
                
                sut = TabBarAppController()
                
                sut.viewControllers = [
                oneViewController,
                twoViewController,
                threeViewController]
                

                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            context("when MainTabBarController is instanciated", {
                it("should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("TabBarAppControllerSpec")
                })
            })
        }
    }
}
