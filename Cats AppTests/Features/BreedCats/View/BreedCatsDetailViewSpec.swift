//
//  BreedCatsDetailViewSpec.swift
//  Cats AppTests
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import Cats_App

class BreedCatsDetailViewSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsDetailViewSpec") {
            
            var sut: BreedCatsDetailView!
            var mainVC: UIViewController!
            beforeEach {
                var delegate: BreedCatsDetailViewDelegate?
                let breeds:[BreedModel] = [BreedModel(id: "abys", name: "Abyssinian", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)")]
                let data:BreedDetailModel = BreedDetailModel(breeds: breeds, url: "https://cdn2.thecatapi.com/images/gCEFbK7in.jpg")
                
                sut = BreedCatsDetailView(delegate: delegate)
                sut.setupView(breedDetail: data)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsDetailViewSpec")
                })
            })
        }
    }
}
