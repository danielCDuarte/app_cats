//
//  BreedCatsListViewSpec.swift
//  Cats AppTests
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import Cats_App

class BreedCatsListViewSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsListViewSpec") {
            
            var sut: BreedCatsListView!
            var mainVC: UIViewController!
            beforeEach {
                var delegate: BreedCatsListViewDelegate?
                let breeds:[BreedModel] = [BreedModel(id: "abys", name: "Abyssinian", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)"),BreedModel(id: "abys", name: "Aegean", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)"),BreedModel(id: "abys", name: "American Bobtail", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)"),BreedModel(id: "abys", name: "American Shorthair", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)"),BreedModel(id: "abys", name: "American Wirehair", description: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", wikipediaUrl: "https://en.wikipedia.org/wiki/Abyssinian_(cat)")]
                
                
                sut = BreedCatsListView(pagenatorIndex:20, limit:0, delegate: delegate)
                sut.setup(breedList:breeds)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsListViewSpec")
                })
            })
        }
    }
}
