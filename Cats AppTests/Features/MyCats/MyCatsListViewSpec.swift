//
//  MyCatsListViewSpec.swift
//  Cats AppTests
//
//  Created by Daniel Crespo Duarte on 16/08/20.
//  Copyright © 2020 Daniel Crespo Duarte. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import Cats_App

class MyCatsListViewSpec: QuickSpec {
    override func spec() {
        describe("MyCatsListViewSpec") {
            
            var sut: MyCatsListView!
            var mainVC: UIViewController!
            beforeEach {
                let LikeCatModelOne = LikeCatModel()
                LikeCatModelOne.name = "MTgxNTk2MQ"
                LikeCatModelOne.createdAt = "2020-08-16T19:51:55.000Z"
                LikeCatModelOne.isLike = true
                LikeCatModelOne.url = "https://cdn2.thecatapi.com/images/MTgxNTk2MQ.jpg"
                
                let LikeCatModelTwo = LikeCatModel()
                LikeCatModelTwo.name = "demo-cd968e"
                LikeCatModelTwo.createdAt = "2020-08-16T19:09:37.000Z"
                LikeCatModelTwo.isLike = false
                LikeCatModelTwo.url = "https://cdn2.thecatapi.com/images/d5m.jpg"
                
                let youLikeList:[LikeCatModel] = [LikeCatModelOne, LikeCatModelTwo]
                sut = MyCatsListView()
                sut.setup(myCatsList: youLikeList)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("MyCatsListViewSpec")
                })
            })
        }
    }
}
